class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s
        
empleadoPepe = Empleado("Pepe", 20000)
#doy nombre a una variable y lo construyo a partir de una clase dandole los parámetros necesario
#el constructor de empleados es el __init__
#cuando ejecuto la linea 8, lamo al constructor y en este caso, he copiado "Pepe" en n
#y 20000 en s. Porque ya he hecho la asignación 
empleadaAna = Empleado("Ana",30000)
print ("El nombre del empleado es {}".format(empleadoPepe.nombre))
