from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse

HOST_ADDRESS = "localhost"
HOST_PORT = 55555

formhtml = """
<!DOCTYPE html>
<html>
<body>
<h2>HTML Forms</h2>
<form action="/action_page">
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname" value="John"><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname" value="Doe"><br><br>
  <input type="submit" value="Submit">
</form>
<p>If you click the "Submit" button, the form-data will be sent to a page called "/action_page".</p>
</body>
</html>"""

responsehtml = """
<!DOCTYPE html>
<html>
<body>
<h2>HTML Forms</h2>
<p> Hola Amigos!</p>
</body>
</html>"""

class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        if (self.path =="/" or self.path == "/index.html"):
            self.wfile.write(formhtml.encode('utf-8'))
        elif (self.path.startswith("/action_page")):
            query = urlparse(self.path).query
            query_components = dict(qc.split("=") for qc in query.split("&"))
            fname = query_components["fname"]
            lname = query_components["lname"]
            print(fname, lname)
            self.wfile.write(responsehtml.encode('utf-8'))


