import unittest
from coche import Coche
coche1 = Coche("rojo","jeep","compact", "457878LKJ", 12)

class TestCoche(unittest.TestCase):
    def test_aceleración1(self):
        a = coche1.acelerar(10)
        self.assertEqual(a, 22)
    def test_aceleración2(self):
        coche1.velocidad = 12
        a = coche1.acelerar(-10)
        self.assertEqual(a, 2)
    def test_frenar1(self):
        coche1.velocidad = 12
        a = coche1.frenar(10)
        self.assertEqual(a, 2)
    def test_frenar2(self):
        coche1.velocidad = 12
        a = coche1.frenar(-10)
        self.assertEqual(a, 22)
unittest.main()