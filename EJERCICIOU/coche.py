class Coche:
    def __init__(self, color,marca,modelo,matricula,velocidad):
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.matricula = matricula
        self.velocidad = velocidad

    def acelerar (self, km):
        self.velocidad  = self.velocidad + km
        return self.velocidad 

    def frenar (self, kilom):
        self.velocidad   = self.velocidad - kilom
        return self.velocidad 

coche1 = Coche("rojo","jeep","compact", "457878LKJ", 12)
print("Primero el coche acelera",coche1.acelerar(4))
print("Después el coche frena",coche1.frenar(20))