#!/usr/bin/python
from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 21080

class miServidor(BaseHTTPRequestHandler):

        # Este metodo gestiona las peticiones GET de HTTP
        def do_GET(self):
                self.send_response(200)
                self.send_header('Content-type','text/html; charset=utf-8')
                self.end_headers()
                # El servidor nos manda la respuesta
                self.wfile.write("Hello World !".encode("utf-8"))
                return

try:
        server = HTTPServer(('', PORT_NUMBER), miServidor)
        print('Started httpserver on port ' , PORT_NUMBER)

        #Wait forever for incoming http requests
        server.serve_forever()

except KeyboardInterrupt:
        print('Control-C received, shutting down the web server')
        server.socket.close()


$ chmod +x servidor1.py
$ ./servidor1.py
