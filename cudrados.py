class Cuadrado:
    """Comentariusss"""
    def __init__(self, lado):
        self.__lado = lado
    def area(self):
        a = self.__lado **2
      
        return a
    def perimetro(self):
        p = self.__lado *4

        return p
    def __str__(self):
        return "El cuadrado con lado {lado} tiene un área de {ar} y un perímetro de {pr}".format(lado=self.__lado,ar=self.area(), pr=self.perimetro())
print(Cuadrado(2))
#Para hacer un print no puedo hacerlo con aquello que he igualado, por ejemplo, lado. Para poder imprimirlo tengo que referirme a self.__lado, si no no me entiende el programa.


class Rectangulo:
    """Esto es la clase de los rectángulos"""
    def __init__ (self, base, altura):
        self.__base = base
        self.__altura = altura

    def area_rectangulo(self):
        arec= self.__base * self.__altura

        return arec
    def perimetro_rectangulo(self):
        prec = self.__altura*2 + self.__base*2

        return prec
    def __str__(self):
        return "El rectángulo con base {base} y altura {altura} tiene un área de {arec} y un perímetro de {prec}".format(base=self.__base ,altura=self.__altura,arec = self.area_rectangulo(), prec = self.perimetro_rectangulo())
print(Rectangulo(2,4))