#!/usr/bin/python
from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 45454

mihtml = '''
<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <h1> Feliz viernes</h1>
        <p style="color:sea;">I am red</p>
        <p style="color:blue;">I am blue</p>
        <p style="font-size:50px;">I am big</p>
        <img src="https://quesosvillasierra.es/wp-content/uploads/2023/02/raza-Murciano-Granadina-raza-Marisma-raza-Malaguena-raza-Canaria-raza-Majorera-raza-Asturiana-de-los-Valles-raza-Palmera.jpg" alt="Trulli" width="500" height="333">

    </body>
</html>'''

class miServidor(BaseHTTPRequestHandler):

        # Este metodo gestiona las peticiones GET de HTTP
        def do_GET(self):
                self.send_response(200)
                self.send_header('Content-type','text/html')
                self.end_headers()
                # Nos retorna la respuesta
                self.wfile.write(mihtml.encode("utf-8"))
                return

try:
        server = HTTPServer(('', PORT_NUMBER), miServidor)
        print('Started httpserver on port ' , PORT_NUMBER)

        #Wait forever for incoming http requests
        server.serve_forever()

except KeyboardInterrupt:
        print('Control-C received, shutting down the web server')
        server.socket.close()
