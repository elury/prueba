#!/usr/bin/python
from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 55555

class miServidor(BaseHTTPRequestHandler):

        # Este metodo gestiona las peticiones GET de HTTP
        def do_GET(self):
                self.send_response(200)
                self.send_header('Content-type','text/html')
                self.end_headers()
                # Nos retorna la respuesta
                with open ('index.html', 'r') as f:
#Cuando ponemos 'r' lo que hace es que lee el fichero. Porque lo preparamos para leer el fichero.
                    content = f.readlines() #Imprime una lista
                    #content_read = f.read() #imprime un string
                    for line in content:
                        self.wfile.write(line.encode("utf-8"))
#Aquí lo que hacemos es que lea la línea.
                    print(content)
                    #print(content_read) imprime en pantalla

try:
        server = HTTPServer(('', PORT_NUMBER), miServidor)
        print('localhost' , PORT_NUMBER)

        #Wait forever for incoming http requests
        server.serve_forever()

except KeyboardInterrupt:
        print('Control-C received, shutting down the web server')
        server.socket.close()
