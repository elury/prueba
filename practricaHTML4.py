from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse

HOST_ADDRESS = "127.0.0.1"
HOST_PORT = 55555

formhtml = """
<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms</h2>

<form action="/damerespuesta">
  <label for="alimento">First name:</label><br>
  <input type="text" id="alimento" name="alimento" value="Patata"><br>
  <label for="nutriente">Last name:</label><br>
  <input type="text" id="nutriente" name="nutriente" value="Carbohidrato"><br><br>
  <input type="submit" value="Submit">
</form>

<p>If you click the "Submit" button, the form-data will be sent to a page called "/action_page".</p>

</body>
</html>"""

responsehtml = """
<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms</h2>

<p> Tu alimento seleccionado es {} y el nutriente es {}</p>
<p> La url es {url} </p>

</body>
</html>"""

class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        if (self.path =="/" or self.path == "/index.html"):
            self.wfile.write(formhtml.encode('utf-8'))
        elif (self.path.startswith("/damerespuesta")):
            query = urlparse(self.path).query
            query_components = dict(qc.split("=") for qc in query.split("&"))
            alimento = query_components["alimento"]
            nutriente = query_components["nutriente"]
            print(alimento, nutriente)
            response = responsehtml.format(alimento, nutriente, url = self.path)
            self.wfile.write(response.encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself

        self.set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        self.wfile.write("POST data is {}".format(post_data ).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """ follows example shown on docs.python.org """
    server_address = (HOST_ADDRESS, HOST_PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == '__main__':
    run(handler_class=RequestHandler)
