from http.server import HTTPServer, BaseHTTPRequestHandler

HOST_ADDRESS = "localhost"
HOST_PORT = 54245

class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        with open ("."+self.path , 'r') as f:
#Cuando ponemos 'r' lo que hace es que lee el fichero. Porque lo preparamos para leer el fichero.
            content_read = f.read() #imprime un string
            self.wfile.write("GET request for {} {}".format(self.path, content_read).encode('utf-8'))
#esto ,lo que hace es que imprime cosas en el navegador 
             #self.wfile.write(content_read.encode('utf-8'))


#wfile es un atributo de la clase en la que estpy 
    def do_POST(self):
        self.set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """ follows example shown on docs.python.org """
    server_address = (HOST_ADDRESS, HOST_PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == '__main__':
    run(handler_class=RequestHandler)
