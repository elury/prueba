import unittest
from fracciones import Fracciones

class TestFraccion(unittest.TestCase):
    def test_suma (self):
        f1 = Fracciones(1,2)
        f2 = Fracciones(3,7)
        suma = f1.suma(f2)

        self.assertEqual(suma, 13/14)

unittest.main()