import unittest
from mul import multiply
from mul import add
class MultiplyTestCase(unittest.TestCase):
#Aquí estoy llamando a unittest para poder ejecutarlo y TestCase es un objeto intrínseco de unittest.
    def test_multiplication_with_correct_values(self):
        mul1 = multiply(5,5)
        self.assertEqual(mul1,25)
    def test_multiplication_with_incorrect_values(self):
        self.assertNotEqual(multiply(5, 5), 24)
    def test_add_with_correct_values(self):
        self.assertEqual(add(5,5),10)
#Si llamo a dos funciones iguales, una sustitiye a la otra, por eso nunca hay que llamarls igual.

if __name__ == '__main__':
    unittest.main()