import socket

IP = "212.128.254.129"
PORT = 8080

# create an INET, STREAMing socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print(s)

try:
    s.connect((IP, PORT))
except OSError:
    print("Socket already used")
    # But first we need to disconnect
    s.close()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((IP, PORT))

print("Read from the server", s.recv(2048).decode("utf-8"))
#Es un programa que nos hace hacer un cliente que puede acceder a servidores y recibir información/ mensajes de ellos